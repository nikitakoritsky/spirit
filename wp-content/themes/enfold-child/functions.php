<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/

function enfold_child_scripts() {
	wp_enqueue_script( 'match-height', get_stylesheet_directory_uri() . '/assets/js/matchHeight.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'enfold-child-js', get_stylesheet_directory_uri() . '/assets/js/child.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'enfold_child_scripts' );
